import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IniciaSesionPage } from '../inicia-sesion/inicia-sesion';

@Component({
  selector: 'page-confirmaci-nregistro-usuario-conductor',
  templateUrl: 'confirmaci-nregistro-usuario-conductor.html'
})
export class ConfirmaciNRegistroUsuarioConductorPage {

  constructor(public navCtrl: NavController) {
  }
  goToIniciaSesion(){
    this.navCtrl.setRoot(IniciaSesionPage);
    this.navCtrl.popToRoot();
  }
}
