import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { DocumentaciNVehCuloPage } from '../documentaci-nveh-culo/documentaci-nveh-culo';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CorekProvider } from '../../providers/corek/corek';
import moment from 'moment';

@Component({
  selector: 'page-datos-del-veh-culo',
  templateUrl: 'datos-del-veh-culo.html'
})
export class DatosDelVehCuloPage {

  public allLugares: any[];
  public allMarcas: any[];
  public allLineas: any[];
  public code_user:any;
  registration; 
  place_regis; 
  brand; 
  line; 
  model;
  user_id;
  form: FormGroup;
  max;
  constructor(public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private corek: CorekProvider, public toastCtrl: ToastController) {
    this.form = formBuilder.group({
      registration: ['', [Validators.required,Validators.pattern(/^[a-zA-Z0-9]*$/)]],
      place_regis: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      line: ['', [Validators.required]],
      model: ['', [Validators.required]],
    });
    console.log(this.navParams.get('code_user'));
    this.code_user = this.navParams.get('code_user'); 
    this.user_id = this.navParams.get('user_id');
    this.max = moment().format('YYYY');
  }

  ionViewDidEnter(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    this.allLugares = [];
    this.allMarcas = [];
    // Get lugares
    const time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query_post' , {condition:{'post_type':'lugares'}, 'event':time + 'query', key: time});
    this.corek.socket.on(time + 'query' , (lugares , key) =>{
      loader.dismiss();
      if(lugares.length > 0){
        for (let lugar of lugares){
          this.allLugares.push(lugar.post_content);
        }
      }else{
        this.allLugares.push('No hay lugares registrados')
      }
    });
    // Get marcas
    this.corek.socket.emit('query_post' , {condition:{'post_type':'marcas'}, 'event':time + 'marcas', key: time});
    this.corek.socket.on(time + 'marcas' , (marcas, key) =>{
      if(marcas.length > 0){
        for (let marca of marcas){
          this.allMarcas.push(marca.post_content);
        }
      }else{
        this.allMarcas.push('No hay marcas registradas')
      }
    });
  }

  onChange(e, marca){
    this.allLineas = [];
    console.log(marca)
    const time = Date.now().toString()+'wwwwww'+Math.random();
    this.corek.socket.emit('query_post' , {condition:{'post_type':marca}, 'event':time + 'marcas', key: time});
    this.corek.socket.on(time + 'marcas' , (lineas, key) =>{
      if(lineas.length > 0){
        for (let linea of lineas){
          this.allLineas.push(linea.post_content);
        }
      }else{
        this.allLineas.push('No hay lineas registradas')
      }
    });
  }
  
  goToDocumentaciNVehCulo(){
    let loader = this.loadingCtrl.create({
      content: "Espere"
    });
    loader.present();
    let nf = Date.now().toString+'prueba'+Math.random();
    this.corek.socket.emit('query_post', { 'condition': {"post_type":"cars", "post_content_filtered":this.form.value.registration} ,'key': 'notificacion', 'event': nf});
    this.corek.socket.on(nf, (data, key) => {
       if(data.length > 0){
         let toast = this.toastCtrl.create({
          message: 'Esta matricula ya se encuentra registrada',
          duration: 6000
        });
        loader.dismiss()
        toast.present();
       }else{
        loader.dismiss();
        let car_data = {'registration': this.form.value.registration, 'place': this.form.value.place_regis, 'brand': this.form.value.brand, 'line': this.form.value.line, 'model': this.form.value.model}
        this.navCtrl.setRoot(DocumentaciNVehCuloPage, {car_data:car_data, user_id: this.user_id, code_user: this.code_user});
        this.navCtrl.popToRoot();
      }
    });
  }
}
