import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController} from 'ionic-angular';
import { RecordarContraseAPage } from '../recordar-contrase-a/recordar-contrase-a';
import { RegistroConductor1Page } from '../registro-conductor1/registro-conductor1';
import { CorekProvider } from '../../providers/corek/corek'
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import { ConfirmaciNDocumentaciNConductorPage } from '../confirmaci-ndocumentaci-nconductor/confirmaci-ndocumentaci-nconductor';
import { PayPage } from '../pay/pay';

@Component({
  selector: 'page-inicia-sesion',
  templateUrl: 'inicia-sesion.html'
})
export class IniciaSesionPage {
  form: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye';
  constructor(private geolocation: Geolocation, public formBuilder: FormBuilder, public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController ,public toastCtrl: ToastController, private storage: Storage, private corek: CorekProvider) {
    this.form = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    this.geolocation.getCurrentPosition().then((result)=>{
      console.log('resultado'+result);
    }).catch((error)=>{
      console.log('el error'+error);
    });
    
  }
  userdata;

  loginUser(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    let datarequest= {
      'email':this.form.value.email,
      'password':this.form.value.password
    }
    if (this.form.value.email == '' || this.form.value.password == ''){
      loader.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Todos los datos son requeridos',
        duration: 5000
      });
      toast.present();
    }else{
      let n = Date.now().toString+'cone';
      this.corek.ConnectCorekconfig(n);
      this.corek.socket.on(n, (dat, key)=>{
        console.log('coneccion'+dat)
        let event = Date.now().toString()+'finishlogin';
        let nf = Date.now().toString()+'createuser';
        this.corek.socket.emit('get_users', {'event': nf, 'key':'validemail', 'condition':{'user_email': datarequest.email}});
        this.corek.socket.on(nf, (data, key)=>{
          this.userdata = data[0];
          this.corek.socket.emit('signon' ,{log:datarequest.email, pwd: datarequest.password,'event':event, 'key':{'key':'login', 'datauser':this.userdata}});
        });
        this.corek.socket.on(event, (data , key)=>{
          if (data.token){
            if (this.userdata.user_status == 0){
              const alert = this.alertCtrl.create({
                title: 'Lo sentimos',
                subTitle: 'Su cuenta  aún no ha sido activada.',
                buttons: ['OK']
              });
              loader.dismiss();
              alert.present();
            }else if ( this.userdata.user_status == 3 ) {
              const alert = this.alertCtrl.create({
                title: 'Lo sentimos',
                subTitle: 'Usted debe iniciar sesión en wintowin cliente.',
                buttons: ['OK']
              });
              loader.dismiss();
              alert.present();

            }else if (this.userdata.user_status == 2){
              const confirm = this.alertCtrl.create({
                title: 'Lo sentimos',
                message: 'Su periodo de licencia se ha vencido',
                buttons: [
                  {
                    text: 'Premium',
                    handler: () => {
                      loader.dismiss();
                      this.navCtrl.setRoot(PayPage, {licenses_end: true});
                      this.navCtrl.popToRoot();
                    }
                  },
                  {
                    text: 'Cancelar',
                    handler: () => {
                      loader.dismiss();
                    }
                  }
                ]
              });
              confirm.present();

            } else if(this.userdata.code_car == undefined){
              const confirm = this.alertCtrl.create({
                title: 'Aún no posees un vehiculo registrado',
                message: 'Te invitamos a que lo hagas para poder ingresar ¿Deseas hacerlo?',
                buttons: [
                  {
                    text: 'Si',
                    handler: () => {
                      this.navCtrl.setRoot(ConfirmaciNDocumentaciNConductorPage, {user_id: this.userdata.ID});
                      this.navCtrl.popToRoot();
                    }
                  },
                  {
                    text: 'No',
                    handler: () => {
                      console.log('cerrado')
                    }
                  }
                ]
              });
              loader.dismiss();
              confirm.present();
            }else{
              this.storage.set('ID', data.ID);
              this.storage.set('user_name', this.userdata.user_url);
              this.storage.set('user_code', this.userdata.user_activation_key);
              this.storage.set('user_car', this.userdata.code_car);
              this.storage.set('user_mail', this.userdata.user_email);
              this.storage.set('user_id', this.userdata.ID);
              this.storage.set('remember', true);  
              this.storage.set('session', data.token);                 
              loader.dismiss();
              this.navCtrl.setRoot(CrearUnViajePage);
              this.navCtrl.popToRoot();
            }
          }else{
            loader.dismiss();
            let toast = this.toastCtrl.create({
              message: 'Los datos ingresados no estan registrados',
              duration: 6000
            });
            toast.present();
          }
        });
      }); 
    }
  }

  showHide(){
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  goToRecordarContraseA(params){
    if (!params) params = {};
    this.navCtrl.push(RecordarContraseAPage);
  }

  goToRegistroConductor(){
    this.navCtrl.push(RegistroConductor1Page);
  }
}
