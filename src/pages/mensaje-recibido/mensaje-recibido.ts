import { Component } from '@angular/core';
import { ResponderMensajePage } from '../responder-mensaje/responder-mensaje';
import { VerUsuarioPage } from '../ver-usuario/ver-usuario';
import { NavController, NavParams} from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek'
import { Storage } from '@ionic/storage'

@Component({
  selector: 'page-mensaje-recibido',
  templateUrl: 'mensaje-recibido.html'
})
export class MensajeRecibidoPage {
public dataUserList: any;
public messages=[];
public name: any;
public photo: any;
public idSendMessage: any;
public myId: any;
trip_id;

constructor(private storage: Storage, private corek: CorekProvider, public navParams: NavParams, public navCtrl: NavController) {
    this.dataUserList = this.navParams.get('dataChatUser');
    this.trip_id = this.navParams.get('trip_id');
    this.showMessage();
  }

  showMessage(){
    this.storage.get('ID').then(id=>{
      this.myId = id;
      const time = Date.now()+Math.random();
      this.name = this.dataUserList.name;
      this.photo = this.dataUserList.photo;
      this.corek.socket.emit('query',{'event':time + 'list', 'querystring':"SELECT * FROM `wp_posts` WHERE `post_parent` = "+this.trip_id+" AND (`wp_posts`.`post_author` = "+this.dataUserList.id_user+" OR `wp_posts`.`post_excerpt` = "+this.dataUserList.id_user+") ORDER BY ID"});
      this.corek.socket.on(time + 'list', (data)=>{
        for(let i in data){
          let msj = data[i].post_content;
          let id = data[i].post_author;
          this.messages.push({'id':id,'msj':msj});
        }
      });
    }); }

  goToResponderMensaje(){
    this.navCtrl.setRoot(ResponderMensajePage, {'idSendMessage':this.dataUserList, 'trip_id':this.trip_id});
    this.navCtrl.popToRoot();
  }
}
