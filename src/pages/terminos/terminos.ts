import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CorekProvider } from "../../providers/corek/corek";

/**
 * Generated class for the TerminosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terminos',
  templateUrl: 'terminos.html',
})
export class TerminosPage {

  public loader:number = 0;
  public terminos:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public corek: CorekProvider) {
  }

  ionViewDidLoad() {
    let conection = Date.now().toString+'conection'+Math.random();
    this.corek.ConnectCorekconfig(conection);
    this.corek.socket.on(conection, (status)=>{
      console.log(status);
      if (status.conf == true){
        this.getTerms();
      }
    });
    console.log('ionViewDidLoad TerminosPage');
  }

  getTerms(){
    const time = Date.now()+'getTerms'+Math.random();
    this.corek.socket.emit('query_post',{'condition':{'post_type':'terms'}, 'event':time});
    this.corek.socket.on(time , (response)=>{
     console.log(response);
     this.loader = 1;
     this.terminos = response[0].post_content; 
    });
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
