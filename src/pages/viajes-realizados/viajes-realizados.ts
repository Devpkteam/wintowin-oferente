import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';
import moment from 'moment'
import { ResultadosViajesRealizadosPage } from '../resultados-viajes-realizados/resultados-viajes-realizados';

@Component({
  selector: 'page-viajes-realizados',
  templateUrl: 'viajes-realizados.html'
})
export class ViajesRealizadosPage {

  // variables de fecha
  from;
  to;
  now;
  // variables de usuario
  id;
  trip;
  constructor(public navCtrl: NavController, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, private corek: CorekProvider) {
    this.now = moment().format('YYYY-MM-DD');

    this.storage.get('user_id').then((id)=>{
      this.id = id;
    });

  }

  buscar(){
    console.log(this.from);
    console.log(this.to);
    this.trip = [];
    let loader = this.loadingCtrl.create({
      content: "Espere"
    });
    loader.present();
    let z = Date.now().toString()+'prueba2'+Math.random();
    this.corek.socket.emit('query',{'querystring':"SELECT * FROM `wp_posts` WHERE `post_author` =" +this.id+"  AND (`post_date` >= '"+this.from+" 00:00:00' AND `post_date` <= '"+this.to+" 23:59:59') AND `post_type` LIKE 'trips' AND `post_parent` = 1", 'event':z});
    this.corek.socket.on(z, (data, key) => {
      console.log(data);
      for (let i in data){
        let date = moment(data[i].post_date).add('hour', 5);
        let data_trip = {origin: JSON.parse(data[i].post_content).name, destiny: JSON.parse(data[i].post_password).name, distance: data[i].to_ping, hour: moment(date).format('hh:mm A'), date: moment(date).format('DD MMM/YY')};
        this.trip.push(data_trip);
      }
      if (this.trip.length == 0){
        loader.dismiss();
        let toast = this.toastCtrl.create({
          message: 'No se encontraron viajes',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }else{
        loader.dismiss();
        this.navCtrl.setRoot(ResultadosViajesRealizadosPage, {'result': this.trip});
        this.navCtrl.popToRoot();
      }
    });
  }

}
