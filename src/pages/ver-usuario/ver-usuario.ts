import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController} from 'ionic-angular';
import { EstadoDelViajePage } from '../estado-del-viaje/estado-del-viaje';
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-ver-usuario',
  templateUrl: 'ver-usuario.html'
})
export class VerUsuarioPage {

  client_data;
  trip_id;
  seats;
  meta_id;
  public empty: any;
  public halfEmpty: any;
  public medium: any;
  public halfFull: any;
  public full: any;
  public average = 0;
  public result = 0;
  public bandera: any;
  public flag: any;  
  public name: any;
  public lastName: any;
  public comment: any;
  public photo: any;
  public items = [];
  public percentage;
  public wins:number = 0;
  id;
  pick;
  message;
  user_email;
  user_name;
  distance;
  show_spinner;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private corek: CorekProvider, private storage: Storage) {
    this.flag = this.navParams.get('flag');

    this.client_data = this.navParams.get('client');
    console.log(this.client_data);
    this.wins = Math.round(parseFloat(this.client_data.distance));
    this.meta_id = this.navParams.get('meta_id');

    this.trip_id = this.navParams.get('trip_id');
    console.log(this.trip_id);
    this.storage.get('ID').then(data=>{
      this.id = data;
    })

  }

  pickPoint(){
    if ( this.trip_id == undefined) {

    }
    // this.myCurrentPosition();
    let time = Date.now().toString()+Math.random();
    console.log(this.client_data);
    console.log(this.trip_id);
    this.corek.socket.emit('query',{'event':time, 'querystring':"SELECT * FROM `wp_posts` WHERE `to_ping` LIKE '"+this.client_data.user_id+"' AND `post_author`="+this.trip_id+" AND `post_type` LIKE 'pickUpPoint' AND `post_title`=1"});
    this.corek.socket.on(time, (data, key)=>{
      console.log(data);
      this.pick = data[0].post_content;
    });
  }
  

  ionViewDidEnter(){
    this.pickPoint();
    console.log(this.client_data)
    if(this.client_data.clients.length >= 8){
      this.message = "";
    }else{
      this.message = "Este usuario ha solicitado reservacion para el y "+(this.client_data.clients.length-1)+" usuarios mas"; 
    }

    this.seeCalification();
    let nf = Date.now().toString()+'createuser'+Math.random();
    this.corek.socket.emit('get_users', {'event': nf, 'key':'validemail', 'condition':{'ID':this.client_data.user_id}});
    this.corek.socket.on(nf, (data, key)=>{
      if (data.length > 0){
        this.user_email = data[0].user_email;
        this.user_name = data[0].user_url; 
      }
    });
  }

  goToEstadoDelViaje(){
    this.navCtrl.setRoot(EstadoDelViajePage, {trip_id: this.trip_id});
    this.navCtrl.popToRoot();
  }

  acceptUser(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    let nf1 = Date.now().toString()+"ins"+Math.random();
    this.corek.socket.emit('query_post', {'condition': {"post_type":"trips", "ID":this.trip_id} , 'key': 'notificacion', 'event': nf1});
    this.corek.socket.on(nf1, (data1, key) => {
      console.log(data1)
      this.distance = data1[0].to_ping;
      console.log(data1[0].to_ping)
      let nf2 = Date.now().toString()+"insert"+Math.random();
      this.seats = data1[0].menu_order;
      if (this.seats != 0){
        if (this.client_data.clients.length >= 8){
          this.seats = this.seats - 1;
        }else{
          this.seats = this.seats - this.client_data.clients.length;
        }
        if (data1[0].post_name == ''){
          this.corek.socket.emit('query',{'event':nf2, 'querystring':"UPDATE wp_posts SET post_name= '"+JSON.stringify(this.client_data)+"', menu_order= "+this.seats+" WHERE post_type="+'"trips"'+' AND ID="'+this.trip_id+'"'});
        }else if(data1[0].post_content_filtered == ''){
          this.corek.socket.emit('query',{'event':nf2, 'querystring':"UPDATE wp_posts SET post_content_filtered= '"+JSON.stringify(this.client_data)+"', menu_order= "+this.seats+" WHERE post_type="+'"trips"'+' AND ID="'+this.trip_id+'"'});
        }else if(data1[0].guid == ''){
          this.corek.socket.emit('query',{'event':nf2, 'querystring':"UPDATE wp_posts SET guid= '"+JSON.stringify(this.client_data)+"', menu_order= "+this.seats+" WHERE post_type="+'"trips"'+' AND ID="'+this.trip_id+'"'});
        }
        this.corek.socket.on(nf2, (data2, key) => {
          //Envio de correo
          this.sendMail();
          //Eliminar postulados
          let nf3 = Date.now().toString()+"inse"+Math.random();
          this.corek.socket.emit('query',{'event':nf3, 'querystring':"DELETE FROM wp_postmeta WHERE wp_postmeta.meta_id = "+this.meta_id});
          this.corek.socket.on(nf3, (data3, key) => {
            var not = {
              notification: {
                title: 'Has sido aceptado en el viaje', 
                body:"Dirigite al punto de recogida",
              },
              android: {
                notification: {
                  color: '#ebc041',
                  sound:'default',
                },
              },
              'topic':'notification'+this.client_data.user_id,
            };
            let n = Date.now().toString()+'cone'+Math.random();
            this.corek.ConnectCorekconfig2(n);
            this.corek.socket.on(n, (dat, key)=>{
              console.log(dat, this.client_data.user_id);
              if (dat.conf == true){
                // Envio de aceptacion
                this.corek.socket.emit('newpost',{"data": {accepted:true, reservacion:false, calification:false, hero: this.id, distance: this.distance, trip_id:this.trip_id}, event:'listen'+this.client_data.user_id});
                // Envio de notificacion
                this.corek.socket.emit('emitnotification', {notification:not});
                loader.dismiss();
                this.navCtrl.setRoot(EstadoDelViajePage, {seats: this.seats, trip_id: this.trip_id});
                this.navCtrl.popToRoot();
              }
            });
          });
        });
      }
    });
  }

  sendMail(){
    console.log('sendMail');
    let nf3 = Date.now().toString()+'send_mail'+Math.random();
    let mail = '<div style="position:relative;min-width:320px;">'+
    '<div class="clearfix borderbox" style="z-index:1;min-height:535px;background-image:none;border-width:0px;border-color:#000000;background-color:transparent;padding-bottom:62px;width:100%;max-width:800px;margin-left:auto;margin-right:auto;">'+
    '<div class="clearfix colelem" style="z-index:2;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="grpelem"><div></div></div>'+
    '<div class="clearfix grpelem" style="z-index:4;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:19.25%;left:7.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¡Hola '+this.user_name+'!</p></div></div>'+
    '<div class="colelem" style="z-index:3;height:133px;opacity:1;filter:alpha(opacity=100);margin-top:91px;position:relative;width:18.13%;margin-left:40.13%; background-size: contain;">'+
    '<img src="http://159.203.82.152/assets/logo.png" alt=""></div>'+
    '<div class="clearfix colelem" style="z-index:13;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:78px;position:relative;width:46.13%;margin-left:26.13%;">'+
    '<p>¡HAS SIDO ACEPTADO POR TU HERO!.</p></div>'+
    '<div class="clearfix colelem" style="z-index:17;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:77px;position:relative;width:46.13%;margin-left:26.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>Debes desplazarte al punto de recogida elegido por ti, recuerda que el Hero no te esperará, por lo tanto debes estar antes que él pase por dicho lugar.</p></div>'+
    '<div class="clearfix colelem" style="background-color:#000000; height:48px; z-index:8;margin-top:111px;width:100%;">'+
    '<div class="grpelem" style="display:inline;float:left; z-index:8;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="fluid_height_spacer"></div></div><div class="clearfix grpelem" style="height: 17px; display:inline;float:left;z-index:9;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:10%;left:43%; ">'+
    '<p>WIN to WIN</p></div></div>'+
    '<div class="clearfix colelem" style="z-index:21;min-height:17px;background-color:transparent;line-height:14px;color:#000000;font-size:12px;font-family:roboto, sans-serif;font-weight:300;margin-top:9px;position:relative;width:45.88%;margin-left:28.38%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¿Tienes dudas? Envíanos un mensaje a soporte@wintowin.co</p></div><div class="verticalspacer" data-offset-top="600" data-content-above-spacer="599" data-content-below-spacer="61" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>'
    +'</div></div>';
    this.corek.socket.emit('confsmtp',{'key':'confsmtp','token':8,'event':nf3});
    this.corek.socket.on(nf3, (data, key)=>{
      console.log(data);
      console.log(data.key);
      if(key == 'confsmtp'){
        console.log('validar aqui');
        this.corek.socket.emit('sendemail',{'event':event,'key':'sendemail',
        'from':'"WinToWin" <info@dappvipaccess.com>',
        'to':this.user_email,
        'subject':'Aceptado en el viaje',
        'html':mail
        });    
        this.corek.socket.on(event, (data , key)=>{
        console.log(data);
        console.log(key);
        });
      }
    });
  }

  seeCalification(){
    this.show_spinner = true;
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    const time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query',{'event':time + 'query', 'querystring':"SELECT * FROM `wp_posts` WHERE `post_type` LIKE 'calification' AND `to_ping` = "+this.client_data.user_id});
    this.corek.socket.on(time + 'query', (data, key) => {
      console.log(data);
      if(data.length > 0){
        for(let i of data){
          let average = i.post_title/i.post_excerpt;
          this.percentage = average*100/5;
          let start;
          if(this.percentage <= 15) start = 0; 
          else if(this.percentage > 15 && this.percentage < 50) start = 1;
          else if(this.percentage == 50) start = 2;
          else if(this.percentage > 50 && this.percentage <= 90) start = 3;
          else if(this.percentage > 90 && this.percentage <= 100) start = 4;
          const time1 = Date.now().toString() + i.post_author+Math.random();
          let r = Math.random();
          this.corek.socket.emit('query',{'event':time1 + 'query1'+r, 'querystring':"SELECT * FROM `wp_users` WHERE `ID` = "+i.post_author});
          this.corek.socket.on(time1 + 'query1'+r, (data1, key) => {
            if (data1.length>0){
              this.name = data1[0].user_url;
              this.lastName = data1[0].user_nicename;
              this.photo = data1[0].display_name;
              this.items.push({'average':average.toFixed(2), 'start':start,'comment':i.post_content, 'name':this.name,'lastName':this.lastName,'photo':this.corek.ipServe()+this.photo});
              this.show_spinner = false;
              loader.dismiss();
            }else{
              loader.dismiss();
            }
          });
        }
      }else{
        loader.dismiss();
        this.show_spinner = false
        this.bandera = true;
      }
    });
  }

}