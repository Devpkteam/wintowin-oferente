import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { ConfirmaciNContraseAPage } from '../confirmaci-ncontrase-a/confirmaci-ncontrase-a'

@Component({
  selector: 'page-recordar-contrase-a',
  templateUrl: 'recordar-contrase-a.html'
})
export class RecordarContraseAPage {

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, private corek: CorekProvider) {}
  email;

  resetPassword(){
    if(this.email == '' || this.email == undefined){
      let toast = this.toastCtrl.create({
        message: 'Debe agregar un correo',
        duration: 2000
      });
      toast.present();
    }
    else{
      let loader = this.loadingCtrl.create({
        content: "Espere"
      });
      loader.present();      
      let nf = Date.now().toString()+'confipri'+Math.random();
      this.corek.ConnectCorekconfig(nf); 
      this.corek.socket.on(nf, (data2, key) =>{
        let nf = Date.now().toString() + 'consult';
        this.corek.socket.emit('get_users', {'event':nf, 'key':'validemail','condition':{'user_email': this.email}}); 
        this.corek.socket.on(nf, (data1)=>{
          console.log(data1);
          if(data1.length > 0){
            let nf1 = Date.now().toString() + 'jdfgv';
            this.corek.socket.emit('get_user_meta', {'event': nf1,'key':'validesfmail','condition':{'user_id': data1[0].ID, 'meta_key':'md5'}}); 
            this.corek.socket.on(nf1, (md5 , k)=>{
              console.log(md5)
              let nf2 = Date.now().toString()+'send_mail';
              let mail = '<div style="position:relative;min-width:320px;">'+
              '<div class="clearfix borderbox" style="z-index:1;min-height:535px;background-image:none;border-width:0px;border-color:#000000;background-color:transparent;padding-bottom:62px;width:100%;max-width:800px;margin-left:auto;margin-right:auto;">'+
              '<div class="clearfix colelem" style="z-index:2;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
              '<div class="grpelem"><div></div></div>'+
              '<div class="clearfix grpelem" style="z-index:4;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:19.25%;left:7.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
              '<p>¡Hola '+data1[0].user_url+'!</p></div></div>'+
              '<div class="colelem" style="z-index:3;height:133px;opacity:1;filter:alpha(opacity=100);margin-top:91px;position:relative;width:18.13%;margin-left:40.13%; background-size: contain;">'+
              '<img src="http://159.203.82.152/assets/logo.png" alt=""></div>'+
              '<div class="clearfix colelem" style="z-index:13;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:78px;position:relative;width:46.13%;margin-left:26.13%;">'+
              '<p style="text-align:center;">Recuperación contraseña</p></div>'+
              '<div class="clearfix colelem" style="z-index:17;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:77px;position:relative;width:46.13%;margin-left:26.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
              '<a href="https://corek.io/app/Recover?token='+md5[0].meta_value+'&amp;p=http://wintowin.com" class="m_-1771036346644381910btn" style="width:207px;height:45px;background:#000000;border-radius:10px;border:none;color:white;margin:20px auto;display:block;text-align:center;line-height:45px;text-decoration:none" target="_blank">Recuperar</a></div>'+
              '<div class="clearfix colelem" style="background-color:#000000; height:48px; z-index:8;margin-top:111px;width:100%;">'+
              '<div class="grpelem" style="display:inline;float:left; z-index:8;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
              '<div class="fluid_height_spacer"></div></div><div class="clearfix grpelem" style="height: 17px; display:inline;float:left;z-index:9;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:10%;left:43%; ">'+
              '<p>WIN to WIN</p></div></div>'+
              '<div class="clearfix colelem" style="z-index:21;min-height:17px;background-color:transparent;line-height:14px;color:#000000;font-size:12px;font-family:roboto, sans-serif;font-weight:300;margin-top:9px;position:relative;width:45.88%;margin-left:28.38%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
              '<p>¿Tienes dudas? Envíanos un mensaje a soporte@wintowin.co</p></div><div class="verticalspacer" data-offset-top="600" data-content-above-spacer="599" data-content-below-spacer="61" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>'
              +'</div></div>';
              this.corek.socket.emit('confsmtp',{'key':'confsmtp','token':8,'event':nf2});
              this.corek.socket.on(nf2, (data2, key)=>{
                console.log(data2);
                if(key == 'confsmtp'){
                  this.corek.socket.emit('sendemail',{'event':event,'key':'sendemail',
                    'from':'"WinToWin" <noreply@wintowin.com.co>',
                    'to':this.email,
                    'subject':'Recuperar contraseña',
                    'html':mail
                  });
                }
                loader.dismiss();
                let toast = this.toastCtrl.create({
                  message: 'Correo enviado, espere unos minutos y verifique su email',
                  duration: 3500
                });
                toast.present();
                this.navCtrl.setRoot(ConfirmaciNContraseAPage);
                this.navCtrl.popToRoot();
              });
            });
          }else{
            loader.dismiss();
            let toast = this.toastCtrl.create({
              message: 'El correo '+this.email+' no existe',
              duration: 2000
            });
            toast.present();
          }
        });
      });
    }
  }
}
