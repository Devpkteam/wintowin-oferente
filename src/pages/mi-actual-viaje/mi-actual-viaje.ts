import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, ToastController, Events } from 'ionic-angular';
import { EstadoDelViajePage } from '../estado-del-viaje/estado-del-viaje';
import { ConfirmaciNCambioDeRutaPage } from '../confirmaci-ncambio-de-ruta/confirmaci-ncambio-de-ruta';
import { CrearUnViajePage } from '../crear-un-viaje/crear-un-viaje'
import { CorekProvider } from '../../providers/corek/corek';
import { Storage } from '@ionic/storage';
import { ListMessagesPage } from '../list-messages/list-messages';

@Component({
  selector: 'page-mi-actual-viaje',
  templateUrl: 'mi-actual-viaje.html'
})
export class MiActualViajePage {

  id;
  code_user;
  show_trip;
  trip_id;
  begin;
  end;
  trip;
  postulates = [];
  pickUpPostulates = [];
  pickUpClientes = [];
  clients = [];
  public flag: any;
  constructor(public events: Events, public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController ,private corek: CorekProvider, private storage: Storage, public toast: ToastController) {
    this.storage.get('user_code').then((key) => {
      this.code_user = key;
    });

    this.storage.get('user_id').then((id)=>{
      this.id = id;
      this.getTrip();
    });
  }

  getTrip(){
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 15000
    });
    loader.present();
    let nf = Date.now().toString+'prueba'+Math.random();
    this.corek.socket.emit('query_post', { 'condition': {"post_type":"trips", "post_parent":0, "post_author": this.id} ,'key': 'notificacion', 'event': nf});
    this.corek.socket.on(nf, (data, key) => {
      console.log(data);
      this.trip = data[0];
      if(this.trip == null){
        this.show_trip = false;
        loader.dismiss();
      }else{
        this.clients = [];
        this.show_trip = true;
        this.trip_id = this.trip.ID;
        this.begin = JSON.parse(this.trip.post_content);
        this.end = JSON.parse(this.trip.post_password);
        this.seeListMessage();
        // Get Postulates
        let nf = Date.now().toString()+"postulates"+Math.random();
        this.corek.socket.emit('field_name',{'field_name':'client','post_id':this.trip_id,'event':nf});
        this.corek.socket.on(nf, (data, key)=>{
          console.log(data);
          this.postulates = [];
          if(data.length>0){
            for (let i in data){
              this.postulates.push(JSON.parse(data[i].meta_value));
            }
          }
        });
        // Get Accepted 
        if (data[0].post_name != '') this.clients.push(JSON.parse(data[0].post_name));
        if (data[0].post_content_filtered != '') this.clients.push(JSON.parse(data[0].post_content_filtered));
        if (data[0].guid != '') this.clients.push(JSON.parse(data[0].guid));
        if (data[0].post_mime_type != '') this.clients.push(JSON.parse(data[0].post_mime_type));
        this.getPick_postulates();
        this.getPick_client();
        loader.dismiss();
      }
    });
  }

  goToCrearViaje(){
    this.navCtrl.setRoot(CrearUnViajePage);
    this.navCtrl.popToRoot();
  }

  goToEstadoDelViaje(){
    this.navCtrl.setRoot(EstadoDelViajePage, {trip_id:this.trip_id});
    this.navCtrl.popToRoot();
  }

  goToConfirmaciNCambioDeRuta(){
    this.events.publish('killBG');
    const confirm = this.alertCtrl.create({
      title: '¡ELIMINARAS TU VIAJE!',
      message: '¿Seguro que deseas continuar?',
      buttons: [
        {
          text: 'Eliminar',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "Espere",
              duration: 15000
            });
            loader.present();
            let nf1 = Date.now().toString()+"insi"+Math.random();
            this.corek.socket.emit('query',{'event':nf1, 'querystring':"DELETE FROM wp_posts WHERE wp_posts.ID="+this.trip_id});
            this.corek.socket.on(nf1, (data1)=>{
              let nf2 = Date.now().toString()+"insi"+Math.random();
              this.corek.socket.emit('query',{'event':nf2, 'querystring':"DELETE FROM wp_postmeta WHERE wp_postmeta.post_id = "+this.trip_id});
              this.corek.socket.on(nf2, (data2)=>{
                console.log(data2)
                let n = Date.now().toString+'cone'+Math.random();
                this.corek.ConnectCorekconfig2(n);
                this.corek.socket.on(n, (dat, key)=>{
                  if (dat.conf == true){
                    if (this.postulates.length>0){
                      let i = 0;
                      for (let postulate of this.postulates){
                        this.updatePickPostulates(i);
                        this.updateUser(postulate);
                        this.sendNotification(postulate.user_id);
                        i++;
                      }
                    }
                    if (this.clients.length>0){
                      let i = 0;
                      for (let client of this.clients){
                        this.updatePickClient(i);
                        this.updateUser(client);
                        this.sendNotification(client.user_id)
                        i++;
                      }
                    }
                    loader.dismiss();
                    this.navCtrl.setRoot(ConfirmaciNCambioDeRutaPage);
                    this.navCtrl.popToRoot();
                  }
                });
              });
            });
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  sendNotification(id){
    var not = {
      notification: {
        title: 'Se ha cancelado el viaje', 
        body:"El viaje que deseabas abordar ha sido cancelado por el Hero",
      },
      android: {
        notification: {
          color: '#ebc041',
          sound:'default',
        },
      },
      'topic':'notification'+id,
    };
    // Envio de cancelacion
    this.corek.socket.emit('newpost',{"data": {accepted:false, reservacion:false, calification:false}, event:'listen'+id});
    // Envio de notificacion
    this.corek.socket.emit('emitnotification', {notification:not});
  }

  goToMessages(){
    console.log(this.flag);
    if (this.flag == true){
      this.navCtrl.setRoot(ListMessagesPage, {trip_id: this.trip_id});
      this.navCtrl.popToRoot();
    }else if(this.flag == false){
      let toast = this.toast.create({
        message: 'No te han enviado mensajes',
        duration: 6000
      });
      toast.present();
    }
  }
  
  seeListMessage(){
    const time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query_post', {'condition': {"post_type":"listMessage", "post_parent":this.trip_id} , 'key': 'notificacion', 'event': time + 'query1'});
    this.corek.socket.on(time + 'query1', (data, key) => {
      console.log(data);
      if(data.length > 0){
        this.flag = true;
      }else{
        this.flag = false;
      }
    });
  }

  getPick_postulates(){
    console.log(this.postulates.length);

    if (this.postulates.length > 0 ) {
      for (let postulate of this.postulates){
        console.log(postulate)
        let time = Date.now().toString()+Math.random();
        this.corek.socket.emit('query',{'event':time, 'querystring':"SELECT ID FROM `wp_posts` WHERE `to_ping` LIKE '"+postulate.user_id+"' AND `post_author`="+this.trip_id+" AND `post_type` LIKE 'pickUpPoint'"});
        this.corek.socket.on(time, (data, key)=>{
          console.log(data);
          this.pickUpPostulates.push(data[0].ID);
        });
      }
    }
  }

  updatePickPostulates(i){
    console.log(this.pickUpPostulates[i]);
  }

  getPick_client() {
    console.log(this.clients.length);
    if (this.clients.length > 0 ) {

      for (let client of this.clients){
        let time = Date.now().toString()+Math.random();
        this.corek.socket.emit('query',{'event':time, 'querystring':"SELECT ID FROM `wp_posts` WHERE `to_ping` LIKE '"+client.user_id+"' AND `post_author`="+this.trip_id+" AND `post_type` LIKE 'pickUpPoint'"});
        this.corek.socket.on(time, (data, key)=>{
          if (data.length>0){
            this.pickUpClientes.push(data[0].ID);
          }
        });
      }
    }
  }

  updatePickClient(i){
    console.log(this.pickUpClientes[i]);
    const time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query',{'event':time+'p', 'querystring':"UPDATE `zadmin_wintowin`.`wp_posts` SET `post_title` = 0 WHERE `wp_posts`.`ID` ="+this.pickUpClientes[i]});
  }

  updateUser(client){
    let updateClient = Date.now()+'updatee'+Math.random();
    this.corek.socket.emit('update_user',{'set':{'id_hero':null, 'id_trip':null},'condition':{'ID':client.user_id}, 'event':updateClient});
  }
}