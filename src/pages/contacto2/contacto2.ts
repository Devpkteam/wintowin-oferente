import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { CorekProvider} from '../../providers/corek/corek';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import moment from 'moment';

@Component({
  selector: 'page-contacto2',
  templateUrl: 'contacto2.html'
})
export class Contacto2Page {
public message : any;
public id : any;
public flag = false;
public messageR = [];
public name: any;
public email: any;

  constructor(public toast: ToastController,public navCtrl: NavController, private storage: Storage, private corek: CorekProvider) {}
    
  ionViewDidEnter(){
    let time = Date.now().toString()+Math.random();
    this.storage.get('ID').then(id=>{
      this.id = id;
      this.corek.socket.emit('query',{'event':time + 'query', 'querystring':"SELECT * FROM `wp_posts` WHERE `post_excerpt` = "+this.id+" AND `post_type` LIKE 'supportAnswer'"});
      this.corek.socket.on(time + 'query', (result, key)=>{
        this.flag = true;
        if(result.length>0){
          for(let i in result){
            let fechaDB = new Date(result[i].post_date);
            let fechaDB1 = fechaDB.setHours(fechaDB.getHours()-4);
            this.messageR.push({'mensaje':result[i].post_content,'fecha':fechaDB1});
          }
        }
      });
    });
  }

  sendMessage(){
    let mjsAux = this.message;
    let toast = this.toast.create({
      message: 'Mensaje enviado al soporte',
      duration: 4000
    });
    let time = Date.now().toString()+Math.random();
    this.corek.socket.emit('query',{'event':time + 'queryU', 'querystring':"SELECT * FROM `wp_users` WHERE `ID` = "+this.id});
    this.corek.socket.on(time + 'queryU', (result, key)=>{
      console.log(result);
      this.email = result[0].user_email;
      this.name = result[0].user_url;
      this.corek.socket.emit('insert_post',{'condition':{'post_type':'support','post_status':'publish', 'post_author':this.id,'post_content':mjsAux, 'post_name':this.name, 'post_title':this.email},'event':time +'inserListMedssage'});
      toast.present();
    });
    this.message = ''; 
  }
}