import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, AlertController, ActionSheetController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormBuilder, FormGroup} from '@angular/forms';
import { IniciaSesionPage } from '../inicia-sesion/inicia-sesion';
import { ConfirmaciNDocumentaciNConductorPage } from '../confirmaci-ndocumentaci-nconductor/confirmaci-ndocumentaci-nconductor';
import { CorekProvider } from '../../providers/corek/corek';
import moment from 'moment';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-registro-conductor2',
  templateUrl: 'registro-conductor2.html'
})
export class RegistroConductor2Page {

  user_data;
  type; number; 
  img_id = ''; 
  img_id1 = ''; 
  date_lice;
  img_lice = '';  
  img_lice1 = '';  
  form: FormGroup;
  min;
  flag;
  constructor(private transfer: FileTransfer, private file: File, private corek: CorekProvider, public toast: ToastController,public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams ,public loadingCtrl: LoadingController , public alertCtrl: AlertController, private camera: Camera , public actionSheetCtrl: ActionSheetController) {
    this.form = formBuilder.group({
      mail1: [''],
      mail2: ['']
    });
    this.user_data = this.navParams.get('user');
    this.min = moment().format("YYYY-MM-DD");
  }

  registerDocs(){
    console.log('registro')
    let loading = this.loadingCtrl.create({
      content: "Espere por favor",
      duration: 15000
    });
    this.flag = 0;
    loading.present();
    let nf = Date.now().toString()+"get_licences"+Math.random();
    console.log('hola');
    this.corek.socket.emit('query',{'event':nf, 'querystring':"SELECT * FROM `wp_usermeta` WHERE `meta_key` LIKE 'identification'"});
    this.corek.socket.on(nf,(data,key)=> {
      console.log('hola');
      console.log(data);
      for (let i in data){
        if(data[i].meta_value!="" && JSON.parse(data[i].meta_value).number == this.number){
          this.flag = 1;
        }
      }
      if (this.flag == 1){
        let toast = this.toast.create({
          message: 'Ese número de identificación ya existe',
          duration: 6000
        });
        loading.dismiss();
        toast.present();
      }else if (this.img_id == null || this.img_id == '' || this.img_id1 == null || this.img_id1 == ''){
          const alert = this.alertCtrl.create({
            title: 'Es necesario su documento de identidad',
            subTitle: 'Necesitamos que cargues el documento de identidad para validar tus datos!',
            buttons: ['OK']
          });
          loading.dismiss();
          alert.present();
      }else if (this.img_lice == null || this.img_lice == '' || this.img_lice1 == null || this.img_lice1 == ''){
        const alert = this.alertCtrl.create({
          title: 'Es necesaria la foto de su licencia',
          subTitle: 'Necesitamos que cargues la licencia para validar tus datos!',
          buttons: ['OK']
        });
        loading.dismiss();
        alert.present();
      }else{
        console.log('consulta de codigo')
        let code_user = this.uuidv4();
        let nf1 = Date.now().toString()+"ins"+Math.random();
        this.corek.socket.emit('get_users',{"condition":{'user_activation_key':code_user}, 'event':nf1});
        this.corek.socket.on(nf1, (data2,key)=>{
          console.log(data2)
          if (data2.length>0){
            code_user = this.uuidv4();
          }
          let fecha = new Date();
          let Dfecha = fecha.setDate(fecha.getDate() + 15);
          let nf2 = Date.now().toString()+"ins"+Math.random();
          this.corek.socket.emit('insert_user',{'insert':{
            'user_email':this.user_data.email, 
            'user_pass':this.user_data.pass, 
            'user_registered': new Date(),
            'user_registered_end':new Date(Dfecha),
            'user_url':this.user_data.name, 
            'user_nicename': this.user_data.lastname, 
            'user_login':this.user_data.phone, 
            'user_activation_key': code_user, 
            'display_name':this.user_data.picture, 
            'wallet':100}, 'event':nf2});
          this.corek.socket.on(nf2, (result,key)=>{
            console.log(result);
            let user_id = result.insertId;
            let nf3 = Date.now().toString()+"ins"+Math.random();
            this.corek.socket.emit('activate_user', {'iduser': user_id, 'key': 'activar','event':nf3});
            this.corek.socket.on(nf3, (data, key) =>{
              let identity = {'type': this.type, 'number': this.number, 'photo': this.img_id, 'photoB':this.img_id1};
              let license = {'date': this.date_lice, 'photo_lice': this.img_lice, 'photo_liceBack': this.img_lice1, 'status':0};
              let emails = {'mail1': this.form.value.mail1, 'mail2': this.form.value.mail2};
              // insercion registro 2
              this.corek.socket.emit('add_user_meta',{'insert':{'user_id':user_id, 'meta_key':'md5', 'meta_value':data.md5}, 'event':nf3+'md5'});
              this.corek.socket.emit('add_user_meta',{'insert':{'user_id':user_id, 'meta_key':'identification', 'meta_value':JSON.stringify(identity)}, 'key':'tel', 'event':nf3+'identity'});
              this.corek.socket.emit('add_user_meta',{'insert':{'user_id':user_id, 'meta_key':'license', 'meta_value':JSON.stringify(license)}, 'key':'tel', 'event':nf3+'license'});
              this.corek.socket.emit('add_user_meta',{'insert':{'user_id':user_id, 'meta_key':'emails', 'meta_value':JSON.stringify(emails)}, 'key':'tel', 'event':nf3+'emails'});
              this.corek.socket.emit('add_user_meta',{'insert':{'user_id':user_id, 'meta_key':'community', 'meta_value':'[""]'}, 'key':'tel', 'event':nf3+'comu'});
              this.sendMail();
              loading.dismiss();
              this.navCtrl.setRoot(ConfirmaciNDocumentaciNConductorPage, {'user_id':user_id, 'code_user':code_user});
              this.navCtrl.popToRoot();
            });
          });
        });
      }
    });
  }

  uuidv4() {
    return 'xxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  sendMail(){
    //Envio de correo
    let nf4 = Date.now().toString()+'send_mail'+Math.random();
    let mail = '<div style="position:relative;min-width:320px;">'+
    '<div class="clearfix borderbox" style="z-index:1;min-height:535px;background-image:none;border-width:0px;border-color:#000000;background-color:transparent;padding-bottom:62px;width:100%;max-width:800px;margin-left:auto;margin-right:auto;">'+
    '<div class="clearfix colelem" style="z-index:2;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="grpelem"><div></div></div>'+
    '<div class="clearfix grpelem" style="z-index:4;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:19.25%;left:7.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¡Hola '+this.user_data.name+'!</p></div></div>'+
    '<div class="colelem" style="z-index:3;height:133px;opacity:1;filter:alpha(opacity=100);margin-top:91px;position:relative;width:18.13%;margin-left:40.13%; background-size: contain;">'+
    '<img src="http://159.203.82.152/assets/logo.png" alt=""></div>'+
    '<div class="clearfix colelem" style="z-index:13;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:78px;position:relative;width:46.13%;margin-left:26.13%;">'+
    '<p>¡BIENVENIDO A NUESTRA PLATAFORMA!</p></div>'+
    '<div class="clearfix colelem" style="z-index:17;min-height:17px;background-color:transparent;line-height:22px;color:#000000;font-size:18px;font-family:roboto, sans-serif;font-weight:700;margin-top:77px;position:relative;width:46.13%;margin-left:26.13%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>Hemos recibido tu registro, en breve serás notificado con tu aceptación al sistema</p></div>'+
    '<div class="clearfix colelem" style="background-color:#000000; height:48px; z-index:8;margin-top:111px;width:100%;">'+
    '<div class="grpelem" style="display:inline;float:left; z-index:8;background-color:#000000;position:relative;margin-right:-10000px;width:100%;">'+
    '<div class="fluid_height_spacer"></div></div><div class="clearfix grpelem" style="height: 17px; display:inline;float:left;z-index:9;min-height:17px;background-color:transparent;line-height:18px;color:#FFFFFF;font-size:15px;font-family:roboto, sans-serif;font-weight:300;position:relative;margin-right:-10000px;width:10%;left:43%; ">'+
    '<p>WIN to WIN</p></div></div>'+
    '<div class="clearfix colelem" style="z-index:21;min-height:17px;background-color:transparent;line-height:14px;color:#000000;font-size:12px;font-family:roboto, sans-serif;font-weight:300;margin-top:9px;position:relative;width:45.88%;margin-left:28.38%;" data-muse-temp-textContainer-sizePolicy="true" data-muse-temp-textContainer-pinning="true">'+
    '<p>¿Tienes dudas? Envíanos un mensaje a soporte@wintowin.co</p></div><div class="verticalspacer" data-offset-top="600" data-content-above-spacer="599" data-content-below-spacer="61" data-sizePolicy="fixed" data-pintopage="page_fixedLeft"></div>'
    +'</div></div>';
    this.corek.socket.emit('confsmtp',{'key':'confsmtp','token':8,'event':nf4});
    this.corek.socket.on(nf4, (data, key)=>{
      console.log(data);
      console.log(data.key);
      if(key == 'confsmtp'){
        console.log('validar aqui');
        this.corek.socket.emit('sendemail',{'event':event,'key':'sendemail',
        'from':'"WinToWin" <noreply@wintowin.com.co>',
        'to':this.user_data.email,
        'subject':'Registro de HERO',
        'html':mail
        });    
        this.corek.socket.on(event, (data , key)=>{
        console.log(data);
        console.log(key);
        });
      }
    });
  }

  upload_id(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Documento de identificacion',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            this.img_id = '';
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_id = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            this.img_id = '';
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_id = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_idBack(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Documento de identificacion',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            this.img_id1 = '';
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_id1 = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            this.img_id1;
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_id1 = namefile;
              }, (err) => {
                alert('Error. Intente de nuevo')
              });
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_license(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Licencia de conducir',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            this.img_lice = '';
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_lice = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            this.img_lice = '';
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_lice = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });;
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  upload_licenseBack(){
    let loading = this.loadingCtrl.create({
      content: "Espere",
      duration: 5000
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Licencia de conducir',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            this.img_lice1 = '';
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 70,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : false
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_lice1 = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });;
              }, (err) => {
                // Handle error
            });
          }
        },{
          text: 'Galeria',
          handler: () => {
            this.img_lice1 = '';
            console.log('Gal clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.FILE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation : false,
              // targetWidth: 200,
              // targetHeight: 200,
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.user_data.email + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
              }
              const fileTransfer: FileTransferObject = this.transfer.create();
              loading.present();
              fileTransfer.upload(imageData, 'http://159.203.82.152/custom-upload.php', options1).then((data) => {
                this.img_lice1 = namefile;
                }, (err) => {
                  alert('Error. Intente de nuevo')
                });;
          });
        }
        }
      ]
    });
    actionSheet.present();
  }

  goToIniciaSesion(params){
    if (!params) params = {};
    this.navCtrl.push(IniciaSesionPage);
  }
}
